#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
//#include "timer.h"

int CountChar(char* s, char c) {
	int count = 0;
	for (int i = 0; s[i]; i++) {
		if (s[i] == c) {
			count++;
		}
	}

	return count;
}

voidFreq(char* s) {
	int max = 0;
	char c_max;
	while (*s) {
		int cnt = CountChar(s, *s);
		if (cnt > max) {
			max = cnt;
			c_max = *s;
		}
		s++;
	}

	printf("%c - %d \n", c_max, max);
}

//vector de frecventa
int fq[26];

void topFreq(char* s) {
	for (int i = 0; s[i]; i++) {
		fq[s[i]]++;
	}

	int max = 0;
	char c;

	for (int i = 0; i < 26; i++) {
		if (fq[i] > max) {
			max = fq[i];
			c = (char)i;
		}
	}

}

int main(void) {
	srand(time(NULL));
	float timp;
	char s[1001];
	int n = 1000;
	for (int i = 0; i < n; i++) {
		s[i] = 'A' + rand() % 26;
	}
	s[n] = 0;
	starton();
	voidFreq(s);
	timp = startoff();
	printf("%f\n", timp);

	return 0;
}