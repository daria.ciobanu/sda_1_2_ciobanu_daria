/*
	ClusterNode:
		address - sir de caractere
		datacenter - sir de caractere
		nrServices - nr de servicii
	Service :
		serviceName - sir de caractere
		serviceID - numeric
		type - production / development

	Lista principala - dinamica
	Lista secundara - dinamica

Functii :
	1) add_node : adauga un nou ClusterNode fara servicii in lista principala(se asigura ca nu exista deja un nod cu aceeasi adresa(address))
	2) add_service : adauga un nou serviciu intr-un Cluster Node existend primind adresa acestuia ca parametru si detaliile noului serviciu (se va asigura si ca serviciul nu mai exista deja in niciun alt Cluster Node pe baza serviceID)
	3) remove_node : sterge un nod din lista principala pe baza address si afiseaza nrServices al acestuia
*/

#include <stdio.h>
#include <string.h>
#include "ClusterNode.h"

int main(void) {
	ClusterNode* lista = NULL;

	add_node(&lista, "192.1.1", "A");
	add_node(&lista, "192.1.2", "B");
	add_node(&lista, "192.1.3", "C");

	// adaugare cluster A
	add_service(lista, "192.1.1", "WEB", 13, "production");
	add_service(lista, "192.1.2", "DataService", 14, "development");

	//adaugare cluster B
	add_service(lista, "192.1.1", "Service_2", 15, "production");
	add_service(lista, "192.1.2", "Service_IDK", 16, "development");

	//adaugare cluster C
	add_service(lista, "192.1.3", "Web_CLuster3", 17, "production");

	printf("Printare lista de cluster nodes	\n \n");

	print_cluster_list(lista);

	printf("\n \nDam remove la un nod : \n \n");

	remove_node(&lista, "192.1.1");


	printf("\n \nPrintare lista de cluster nodes	dupa remove\n \n");

	print_cluster_list(lista);

	return 0;
}

