#ifndef CLUSTERLIST_H
#define CLUSTERLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Service {
    char serviceName[31];
    int serviceID;
    char type[20]; // "production" sau "development"
    struct Service* next;
} Service;

typedef struct ClusterNode {
    char address[31];
    char datacenter[31];
    int nrServices;
    struct Service* services; 
    struct ClusterNode* next;
} ClusterNode;

ClusterNode* create_cluster_node(const char* address, const char* datacenter);
Service* create_service(const char* serviceName, int serviceID, const char* type);

void add_node(ClusterNode** head, const char* address, const char* datacenter);
void add_service(ClusterNode* head, const char* address, const char* serviceName, int serviceID, const char* type);
void remove_node(ClusterNode** head, const char* address);

void print_cluster_list(ClusterNode* head);

//functie auxiliara de cautare
int if_exists(int if_service_must_be_searched, ClusterNode* temp, const char* address, const char* serviceName, int serviceID, const char* type);

#endif 
