#include "ClusterNode.h"

ClusterNode* create_cluster_node(const char* address, const char* datacenter) {
	ClusterNode	*node = (ClusterNode*)malloc(sizeof(ClusterNode));

	//verificare 
	if (!node) {
		perror("Eroare alocare memorie pentru ClusterNode");
		exit(EXIT_FAILURE);
	}

	strcpy(node->address, address);
	strcpy(node->datacenter, datacenter);

	node->nrServices = 0;
	node->services = NULL;
	node->next = NULL;

	return node;
}
Service* create_service(const char* serviceName, int serviceID, const char* type) {
	Service* service = (Service*)malloc(sizeof(Service));

	//verificare
	if (!service) {
		perror("Eroare alocare memorie pentru Service");
		exit(EXIT_FAILURE);
	}

	strcpy(service->serviceName, serviceName);
    service->serviceID = serviceID;
	strcpy(service->type, type);

	service->next = NULL;

	return service;
}

void add_node(ClusterNode** head, const char* address, const char* datacenter) {
    // to do functie auxiliara pentru cautare
    ClusterNode* temp = *head;

    while (temp != NULL) {
        if (strcmp(temp->address, address) == 0) {
            printf("Nodul exista deja \n");
            return;
        }

        temp = temp->next;
    }

    // Creare nod nou
    ClusterNode* nod_de_adaugat = create_cluster_node(address, datacenter);

    nod_de_adaugat->next = *head;
    *head = nod_de_adaugat;
}

void add_service(ClusterNode* head, const char* address, const char* serviceName, int serviceID, const char* type) {
    // to do functie auxiliara pentru cautare
    ClusterNode* temp = head;
    while (temp != NULL) {
        if (strcmp(temp->address, address) == 0) {
            Service* service = temp->services;
            while (service != NULL) {
                if (service->serviceID == serviceID) {
                    printf("Serviciul exista deja in lista\n");
                    return;
                }
                service = service->next;
            }
            Service* new_service = create_service(serviceName, serviceID, type);

            new_service->next = temp->services;
            temp->services = new_service;
            temp->nrServices++;

            return;
        }
        temp = temp->next;
    }
    printf("Nu am gasit ClusterNode-ul cu adresa %s\n", address);
}

void remove_node(ClusterNode** head, const char* address) {
    ClusterNode* temp = *head;
    ClusterNode* prev = NULL;

    while (temp != NULL && strcmp(temp->address, address) != 0) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) {
        printf("Nu exista ClusterNode cu adresa %s\n", address);
        return;
    }

    if (prev == NULL) {
        *head = temp->next;
    }
    else {
        prev->next = temp->next;
    }

    printf("S-a dat remove la ClusterNode-ul cu adresa %s si nr de servicii egal cu: %d \n",
        temp->address, temp->nrServices);

    Service* service = temp->services;
    while (service != NULL) {
        Service* to_free = service;
        service = service->next;
        free(to_free);
    }
    free(temp);
}

void print_cluster_list(ClusterNode* head) {

    if (head == NULL) {
        printf("Lista este goala.\n");
        return;
    }

    ClusterNode* temp = head;
    while (temp != NULL) {
        printf("ClusterNode: %s, Datacenter: %s, Services: %d\n", temp->address, temp->datacenter, temp->nrServices);

        Service* service = temp->services;
        while (service != NULL) {
            printf("  Service: %s, ID: %d, Type: %s\n", service->serviceName, service->serviceID, service->type);
            service = service->next;
        }
        temp = temp->next;
    }
}



//asta ar trebui sa fie o functie de cautare dar n-am terminat
int if_exists(int if_service_must_be_searched, ClusterNode* temp, const char* address, const char* serviceName, int serviceID, const char* type) {
    while (temp != NULL) {
        if (strcmp(temp->address, address) == 0) {
            printf("Nodul exista deja \n");
            return 0;
        }
        if (if_service_must_be_searched) {
            Service* service = temp->services;
            while (service != NULL) {
                if (service->serviceID == serviceID) {
                    printf("Serviciul exista deja in lista\n");
                    return 0;
                }
                service = service->next;
            }
            Service* new_service = create_service(serviceName, serviceID, type);
            new_service->next = temp->services;
            temp->services = new_service;
            temp->nrServices++;
        }
        temp = temp->next;
    }

    return 1;
}