#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Msg {
    unsigned int prio;
    char payload[256];
    unsigned long size;
    char rq; // asta poate fi 0 sau 1
} Msg_t;

int cmp(Msg_t a, Msg_t b) {
    if (a.rq != b.rq) {
        return a.rq - b.rq;
    }
    if (a.prio != b.prio) {
        return b.prio - a.prio; 
    }
    if (a.size != b.size) {
        return a.size - b.size;
    }
    return strcmp(a.payload, b.payload);
}

void insertion_sort(Msg_t* arr, int n) {
    int i, j;
    Msg_t tmp;

    for (i = 1; i < n; i++) {
        tmp = arr[i];
        for (j = i; (j > 0) && cmp(tmp, arr[j - 1]) < 0; j--) {
            arr[j] = arr[j - 1];
        }
        arr[j] = tmp;
    }
}

void swap(Msg_t* a, Msg_t* b) {
    Msg_t tmp = *a;
    *a = *b;
    *b = tmp;
}

void quicksort(Msg_t a[], int prim, int ultim) {
    int stanga = prim + 1;
    int dreapta = ultim;
    int mijloc = (prim + ultim) / 2;

    Msg_t v[3] = { a[prim], a[ultim], a[mijloc] };
    insertion_sort(v, 3);

    swap(&a[prim], &v[1]);
    Msg_t pivot = a[prim];

    while (stanga <= dreapta) {
        while (cmp(a[stanga], pivot) < 0) 
            stanga++;
        while (cmp(pivot, a[dreapta]) < 0) 
            dreapta--;
        if (stanga < dreapta)
            swap(&a[stanga++], &a[dreapta--]);
        else 
            stanga++;
    }
    swap(&a[dreapta], &a[prim]);

    if (prim < dreapta - 1) 
           quicksort(a, prim, dreapta - 1);
    if (dreapta + 1 < ultim) 
        quicksort(a, dreapta + 1, ultim);
}

void quicksort_modified(Msg_t a[], int prim, int ultim) {
    if (ultim - prim <= 5) {
        insertion_sort(&a[prim], ultim - prim + 1);
        return;
    }

    int stanga = prim + 1;
    int dreapta = ultim;
    int mijloc = (prim + ultim) / 2;

    Msg_t v[3] = { a[prim], a[ultim], a[mijloc] };
    insertion_sort(v, 3);

    swap(&a[prim], &v[1]);
    Msg_t pivot = a[prim];

    while (stanga <= dreapta) {
        while (cmp(a[stanga], pivot) < 0) 
            stanga++;
        while (cmp(pivot, a[dreapta]) < 0) 
            dreapta--;
        if (stanga < dreapta) 
            swap(&a[stanga++], &a[dreapta--]);
        else 
            stanga++;
    }
    swap(&a[dreapta], &a[prim]);

    if (prim < dreapta - 1) 
        quicksort_modified(a, prim, dreapta - 1);
    if (dreapta + 1 < ultim) 
        quicksort_modified(a, dreapta + 1, ultim);
}


void printArray(Msg_t* arr, int n) {
	for (int i = 0; i < n; i++) {
		printf("rq: %d < - > prio: %u < - > size: %lu < - > payload: %s\n", arr[i].rq, arr[i].prio, arr[i].size, arr[i].payload);
	}

}

int main(void) {
	struct Msg messages[] = {
		{5, "AA", 120, 1},
		{3, "BA", 100, 0},
		{7, "AB", 50, 0},
		{3, "CD", 100, 0},
		{4, "DC", 120, 1}
	};

	int n = sizeof(messages) / sizeof(messages[0]);

	printf("Array inainte de sortare : \n");

	printArray(messages, n);

    //insertion_sort(messages, n);
    quicksort(messages, 0, n - 1);
    //quicksort_modified(messages, 0, n - 1);

	printf("\n Array dupa de sortare : \n");

	printArray(messages, n);


	return 0;
}