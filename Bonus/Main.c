﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void createPattern(const char* pattern, int* buffer, int length) {
    int prefixLength = 0;
    buffer[0] = 0;
    int pos = 1;

    while (pos < length) {
        if (pattern[pos] == pattern[prefixLength] || pattern[pos] == '*' || pattern[prefixLength] == '*') {
            prefixLength++;
            buffer[pos] = prefixLength;
            pos++;
        }
        else {
            if (prefixLength > 0) {
                prefixLength = buffer[prefixLength - 1];
            }
            else {
                buffer[pos] = 0;
                pos++;
            }
        }
    }
}

int characterMatch(char textChar, char patternChar) {
    return patternChar == '*' || textChar == patternChar;
}

void locateMatches(const char* text, const char* pattern, int* positions, int* matchCount) {
    int textLen = strlen(text);
    int patternLen = strlen(pattern);

    if (patternLen == 0) return;

    int* buffer = (int*)malloc(patternLen * sizeof(int));
    createPattern(pattern, buffer, patternLen);

    int textIdx = 0, patternIdx = 0;

    while (textIdx < textLen) {
        if (characterMatch(text[textIdx], pattern[patternIdx])) {
            textIdx++;
            patternIdx++;
        }

        if (patternIdx == patternLen) {
            positions[*matchCount] = textIdx - patternIdx;
            (*matchCount)++;
            patternIdx = buffer[patternIdx - 1];
        }
        else if (textIdx < textLen && !characterMatch(text[textIdx], pattern[patternIdx])) {
            if (patternIdx != 0) {
                patternIdx = buffer[patternIdx - 1];
            }
            else {
                textIdx++;
            }
        }
    }

    free(buffer);
}

void processFile(const char* pattern, const char* filePath) {
    FILE* file = fopen(filePath, "r");
    if (!file) {
        fprintf(stderr, "Error: Could not open file %s\n", filePath);
        return;
    }

    char buffer[1024];
    char* longestMatch = NULL;
    size_t longestLength = 0;

    int* positions = NULL;
    int matchCount = 0;
    int lineIndex = 0;

    while (fgets(buffer, sizeof(buffer), file)) {
        lineIndex++;

        buffer[strcspn(buffer, "\n")] = '\0';

        positions = (int*)malloc(strlen(buffer) * sizeof(int));
        matchCount = 0;

        locateMatches(buffer, pattern, positions, &matchCount);

        for (int i = 0; i < matchCount; i++) {
            printf("Match found at line %d, position %d: \"%s\"\n", lineIndex, positions[i], buffer + positions[i]);
        }

        if (matchCount > 0) {
            size_t currentLength = strlen(buffer);
            if (currentLength > longestLength) {
                longestLength = currentLength;
                if (longestMatch) {
                    free(longestMatch);
                }
                longestMatch = (char*)malloc((currentLength + 1) * sizeof(char));
                if (longestMatch) {
                    strcpy(longestMatch, buffer);
                }
            }
        }

        free(positions);
    }

    if (longestMatch) {
        printf("Longest matching line: %s\n", longestMatch);
        free(longestMatch);
    }
    else {
        printf("No matches found.\n");
    }

    fclose(file);
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <pattern> <file_path>\n", argv[0]);
        return -1;
    }

    const char* pattern = argv[2];
    const char* filePath = argv[1];

    processFile(pattern, filePath);

    return 0;
}
