n = linspace(1, 10, 100);


f1 = 4 * n.^2;         % 4n^2
f2 = log(n) / log(3);  % log3(n)
f3 = 3.^n;             % 3^n
f4 = 20 * n;           % 20n
f5 = 2 * ones(size(n));% Constanta 2
f6 = log(n) / log(3);  % log3(n) (repetat)
f7 = n.^(2/3);         % n^(2/3)

figure;
hold on;
plot(n, f1, 'DisplayName', '4n^2');
plot(n, f2, 'DisplayName', 'log_3(n)');
plot(n, f3, 'DisplayName', '3^n');
plot(n, f4, 'DisplayName', '20n');
plot(n, f5, 'DisplayName', '2 (constant)');
plot(n, f6, '--', 'DisplayName', 'log_3(n) (repetat)');
plot(n, f7, 'DisplayName', 'n^{2/3}');


set(gca, 'YScale', 'log'); 
xlabel('n');
ylabel('Valoarea functiei');
title('Reprezentarea functiilor');
legend('show');
grid on;
hold off;
