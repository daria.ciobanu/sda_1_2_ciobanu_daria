#include <stdio.h>
#include <stdlib.h>


typedef struct {
	int cheie;
	//int valoare;
} Element_t;

void InsertionSortReversed(Element_t a[], int n){
	int i, j;

	Element_t tmp;

	for (i = n - 2; i >= 0; i--) {
		tmp = a[i];   // elementul curent pe care il salvam

		for (j = i ; j < (n - 1) && tmp.cheie > a[j + 1].cheie; j ++) {
			a[j] = a[j + 1];
		}

		a[j] = tmp;
	}
}

int main(void) {
	Element_t a[] = { { 3 }, { 1 }, { 4 }, { 1 }, { 5 }, { 9 }, { 2 }, { 6 }, { 5 }, { 3 } };

	int n = sizeof(a) / sizeof(a[0]);

	InsertionSortReversed(a, n);

	for (int i = 0; i < n; i++) {
		printf("%d ", a[i].cheie);
	}

	return 0;
}